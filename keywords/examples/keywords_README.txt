agrochemicals_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via TOC Browser => Entrez
195	Agrochemical Information
Keywords: agrochemical, pesticide

food_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via TOC Browser => Entrez
208	Food Additives and Ingredients 
Keywords: food

drugs_20200723.txt
CID	SMILES
Download 23/7/2020 via TOC Browser => Entrez
82	Drug and Medication Information
Keywords: pharmaceutical, drug, medication, medicine

pfas_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via SLE Browser => Entrez. 
4 SLE datasets (S09,S14,S25,S49) merged in R into one file. 
Keywords: PFAS, pfas, perfluorinated, polyfluorinated, per- and polyfluoroalkyl substances

phthalates_wiki_20200723.txt
CID	SMILES
Exported from Wikipedia ~20/07/2020 and mapped to CIDs
https://en.wikipedia.org/wiki/Phthalate#Table_of_the_most_common_phthalates
Keywords: phthalate

phthalates_pubchem_20200723.txt
CID	SMILES
Downloaded from SMARTS search, CIDs mapped to SMILES via IDExchange
https://pubchem.ncbi.nlm.nih.gov/#query=COC(%3DO)c1ccccc1C(%3DO)OC&input_type=smarts&fullsearch=true&page=1
SMARTS: COC(=O)c1ccccc1C(=O)OC
NOTE: contains mixtures
Keywords: phthalate

cosmetics_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via SLE Browser => Entrez. 
1 SLE dataset (S13) from top of tree.
Keywords: cosmetics, PCP, personal care products

bisphenols_20200723.txt
CID	SMILES
Downloaded 23/07/2020 via SLE Browser => Entrez
1 SLE dataset (S20).
Keywords: bisphenols

gcms_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via TOC Browser => Entrez
17	GC-MS	Mass Spectrometry	Spectral Information
Keywords: gcms, gc-ms, gas chromatography

lcms_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via TOC Browser => Entrez
483	LC-MS	Mass Spectrometry	Spectral Information
NOTE: merge with msms?
Keywords: lcms, lc-ms, liquid chromatography

msms_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via TOC Browser => Entrez
63	MS-MS	Mass Spectrometry	Spectral Information
NOTE: merge with lc-ms?
Keywords: msms, MS/MS, tandem mass spectrometry

transformations_20200723.txt
CID	SMILES
Downloaded 23/7/2020 via TOC Browser => Entrez
506	Transformations	Pharmacology and Biochemistry
Keywords: TPs, transformation products, transformations
