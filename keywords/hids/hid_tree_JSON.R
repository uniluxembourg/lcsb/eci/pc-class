# playing around with classification trees
# E. Schymanski, 30/10/2020
# Plus Evan Bolton and Paul Thiessen


#### Background info from Paul / Evan #####
# Example
# https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?hid=101
# https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?format=json&hid=101&hnid=4041191&cache_uid_type=cid
# https://pubchem.ncbi.nlm.nih.gov/list_gateway/list_gateway.cgi?format=json&action=cache_get&cache_key=UhX0mAgqbZZavG-l7d0mtIuf2v99qaTN3ui_gcX5rYDF4JE
# get only "shallow" trees to address issues with big trees (hid=1, 2)
# https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?hid=2&depth=2&format=json
# debugging URL from Paul: 
# https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?format=json&hid=14&search_uid=2336983&search_uid_type=hnid&search_type=list
# summary of all trees (WiP)
# https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?format=json

#### Set up dirs, libraries etc ######

library(rjson)
library(httr)

kw_dir <- "C:/DATA/PubChem/keywords/"
git_kw_dir <- "C:/Users/emma.schymanski/Documents/GitHub/pubchem/annotations/keywords/"
setwd(kw_dir)

hid_fn_file_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/annotations/keywords/hids/hid_tree_functions.R"
download.file(hid_fn_file_url,paste0(kw_dir,"hid_tree_functions.R"))
source(paste0(kw_dir,"hid_tree_functions.R"))

kw_summary_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/annotations/keywords/hids/keyword_mapping.csv"
kw_summary_file <- paste0(kw_dir,"keyword_mapping_fromECIgit.csv")
download.file(kw_summary_url,kw_summary_file)

kw_extras_url <- "https://git-r3lab.uni.lu/eci/pubchem/-/raw/master/annotations/keywords/hids/keyword_extra_mapping.csv"
kw_extras_file <- paste0(kw_dir,"keyword_extra_mapping_fromECIgit.csv")
download.file(kw_extras_url,kw_extras_file)


#### systematically extract data from classification trees #####

# Summary

# # to create a summary file:
# hid_summary_file <- getPcHidTreeList()
# to load one that already exists:
hid_summary_file <- "HID_Tree_Summary_1_to_103.csv"
hid_info <- read.csv(hid_summary_file,stringsAsFactors = F)
# remove NAs
hid_info <- hid_info[which(!is.na(hid_info$SourceName)),]
# # write out this file so we can go through titles
# write.csv(hid_info,sub(".csv","_noNAs.csv",hid_summary_file),row.names = F)
#filter also by those with CIDs only
hid_info <- hid_info[which(hid_info$hasCIDs),]
# # write out this file so we can go through titles
# write.csv(hid_info,sub(".csv","_noNAs_wCIDs.csv",hid_summary_file),row.names = F)
# all trees look interesting so let's stick with these ... 

#Create a subfolder for the tree csvs
tree_dir <- paste0(kw_dir,"trees/")

if (!dir.exists(tree_dir)) {
  dir.create(tree_dir)
}

#### Creating the Tree Files #####
# Uncomment this section first time this is run - or to recreate 

# ## Uncomment here if the tree files have not been created
# # save all these files into tree_dir
# setwd(tree_dir)
# 
# for (i in 1:length(hid_info$HID)) {
#   getPcHidTree(hid_info$HID[i],depth=3)
# }
# # set back to kw_dir
# setwd(kw_dir)

# # now how to merge?
# 
# 
# #setwd(tree_dir)
# #tree_files <- list.files(tree_dir,full.names = T)
# tree_files <- list.files(tree_dir,full.names = T,pattern = "_depth3")
# 
# tree_info <- read.csv(tree_files[1],stringsAsFactors = F)
# tree_info1 <- read.csv(tree_files[2],stringsAsFactors = F)
# tree_info <- merge(tree_info,tree_info1,all=T)
# 
# for (i in 3:length(tree_files)) {
#   tree_info1 <- read.csv(tree_files[i],stringsAsFactors = F)
#   tree_info <- merge(tree_info,tree_info1,all=T)
# }
# 
# # write.csv(tree_info,sub("_Summary_","_NodeInfo_depth2_hids_",hid_summary_file),
# #           row.names = F)
# write.csv(tree_info,sub("_Summary_","_NodeInfo_depth3_hids_",hid_summary_file),
#           row.names = F)
# 
# # get rid of those without CIDs: 
# nonZeroNodes <- which(tree_info$node_nCIDs>0)
# tree_info <- tree_info[nonZeroNodes,]
# 
# write.csv(tree_info,sub("_Summary_","_NodeInfo_depth3_hids_nonZeroNodes_",hid_summary_file),
#           row.names = F)
# # 32762 rows with all entries
# # 18058 removing those with zero nCIDs

## now get the info files per keyword
tree_info_file <- "HID_Tree_NodeInfo_depth3_hids_nonZeroNodes_1_to_103.csv"
#kw_summary_file # This is defined above

##### Getting Summary Files for Each Keyword #####

# Now get the summary files for each keyword
kw_info_dir <- getKWInfo(tree_info_file,kw_summary_file)

# now change to the dir containing validated files
kw_info_val_dir <- "C:/DATA/PubChem/keywords/kw_info_validated/"
#kw_info_filenames <- list.files(kw_info_dir, full.names = T)
kw_info_filenames <- list.files(kw_info_val_dir, full.names = T)

## create a summary file over all validated files
for (i in 1:length(kw_info_filenames)) {
  kw <- strsplit(basename(kw_info_filenames[i]),"_")[[1]][1]
  if (i==1) {
    kw_info <- read.csv(kw_info_filenames[i])
    kw_info$kw <- kw
    kw_info <- kw_info[,c(12,1:11)]
  } else {
    kw_info2 <- read.csv(kw_info_filenames[i])
    kw_info2$kw <- kw
    kw_info2 <- kw_info2[,c(12,1:11)]
    kw_info <- merge(kw_info,kw_info2,all=T)
  }
}
write.csv(kw_info,paste0(kw_dir,"all_keyword_summary.csv"),
          row.names = F)
file.copy(paste0(kw_dir,"all_keyword_summary.csv"),
          paste0(git_kw_dir,"all_keyword_summary.csv"))
# # if false, then overwrite but only if you are sure!!!
# file.copy(paste0(kw_dir,"all_keyword_summary.csv"),
#           paste0(git_kw_dir,"all_keyword_summary.csv"),
#           overwrite=T)


kw_cid_dir <- "C:/DATA/PubChem/keywords/kw_cid_files/"

# select certain file names (e.g. earlier files already processed)
#kw_info_filenames <- kw_info_filenames[c(-1,-3,-5,-8,-9,-16)]
# processing PMT list only
#kw_info_filenames <- kw_info_filenames[14]
kw_info_filenames[c(-1,-5,-6,-9,-11,-17,-19,-22,-23,-27,-30,-32,-37,-39,-43,-47,-50)]


### Now get the CIDs for the keywords
### This is THE RATE LIMITING STEP - takes several minutes for the 
# test set of 6 keywords ... 
# test for a name with a space
#getKWInfoCIDs(kw_info_filenames[10])
for (i in 1:length(kw_info_filenames)) {
  getKWInfoCIDs(kw_info_filenames[i])
}

kw_cid_filenames <- list.files(kw_cid_dir,full.names = T)

kw_cid_dir <- "C:/DATA/PubChem/keywords/kw_cid_files/"
#note: added a phthalate one manually 
kw_dir <- "C:/DATA/PubChem/keywords/"

# create with default filename
createKwCIDfile(kw_cid_dir,kw_dir)
# or give a testing file name
#createKwCIDfile(kw_cid_dir,kw_dir,"testing_kw_filegen2.txt")
# files getting to big for git ... copying to Zenodo ...
# file.copy("all_keyword_cids.txt",paste0(git_kw_dir,"all_keyword_cids.txt"))
# # if false, then overwrite but only if you are sure!!!
# file.copy("all_keyword_cids.txt",paste0(git_kw_dir,"all_keyword_cids.txt"),
#           overwrite=T)

# now create the kw_tooltips (currently residing in kw_summary_file)
#kw_summary <- read.csv("keyword_mapping.csv", stringsAsFactors = F)
kw_summary <- read.csv(paste0(git_kw_dir,"hids/keyword_mapping.csv"),
                       stringsAsFactors = F)
# merge manual entries into this ... 
kw_extra_summary <- read.csv(paste0(git_kw_dir,"hids/keyword_extra_mapping.csv"),
                             stringsAsFactors = F)
kw_tooltips <- merge(kw_summary,kw_extra_summary,all=T)

write.table(kw_tooltips[,c(1,4)],paste0(git_kw_dir,"keyword_tooltips.txt"),
            sep="\t",row.names = F,quote=T)
#write CSV of merged file for Zenodo
write.csv(kw_tooltips,paste0(git_kw_dir,"all_keyword_mapping.csv"),
          row.names = F)


###### CODE BELOW HERE IS OBSELETE NOW ######

##### Making sense of categories ######

# generifying the category-specific code
# read in our test file
kw_summary <- read.csv("keyword_mapping.csv",stringsAsFactors = F)

j <- 5
kw_pattern <- strsplit(kw_summary$Keyword_RegEx[j],"|",fixed=T)[[1]]

kw_info <- tree_info[grep(kw_pattern,tree_info$nodeNames),]

i <- 5
for (i in 1:length(kw_pattern)) {
  temp_info <- tree_info[grep(kw_pattern[i],tree_info$nodeNames),]
  if (i == 1) {
    # this relies on the first entry always returning a pattern match
    kw_info <- temp_info
#  } else if (!is.na(temp_info[1])) {
  } else if (dim(temp_info)[1]>0) {
    #kw_list <- c(temp_list,drug_list)
    kw_info <- merge(kw_info,temp_info,all=T)
  }
}
length(kw_info$nodeNames)
# merging takes care of uniqueness
#length(unique(kw_info)$nodeNames)
# unique(drug_list)
# length(unique(drug_list))
kw_info_filename <- paste0(kw_summary$Keyword[j],"_keyword_summary.csv")
write.csv(kw_info,kw_info_filename,row.names = F)

tree_info_file <- "HID_Tree_NodeInfo_depth3_hids_nonZeroNodes_1_to_103.csv"
kw_summary_file <- "keyword_mapping.csv"
getKWInfo(tree_info_file,kw_summary_file)
# now try to do this over all
## first functionalize the csv creation, then loop over. 
# need tree info file, and keyword summary file
getKWInfo <- function(tree_info_file, kw_summary_file) {
  tree_info <- read.csv(tree_info_file,stringsAsFactors = F)
  kw_summary <- read.csv(kw_summary_file,stringsAsFactors = F)
  kw_info_dir <- paste0(getwd(),"/kw_info/")
  if (!dir.exists(kw_info_dir)) {
    dir.create(kw_info_dir)
  }
  # now, get the patterns
  #kw_patterns <- kw_summary$Keyword_RegEx
  
  for (j in 1:length(kw_summary$Keyword_RegEx)) {
    kw_pattern <- strsplit(kw_summary$Keyword_RegEx[j],"|",fixed=T)[[1]]
    
    for (i in 1:length(kw_pattern)) {
      temp_info <- tree_info[grep(kw_pattern[i],tree_info$nodeNames),]
      if (i == 1) {
        # this relies on the first entry always returning a pattern match
        kw_info <- temp_info
        #  } else if (!is.na(temp_info[1])) {
      } else if (dim(temp_info)[1]>0) {
        #kw_list <- c(temp_list,drug_list)
        kw_info <- merge(kw_info,temp_info,all=T)
      }
    }
    length(kw_info$nodeNames)
    # merging takes care of uniqueness
    kw_info_filename <- paste0(kw_info_dir,kw_summary$Keyword[j],"_keyword_summary.csv")
    write.csv(kw_info,kw_info_filename,row.names = F)
  }
  # return the file name
  print_msg <- paste0("Files printed into keyword info dir")
#  return(kw_info_filename)
  return(kw_info_dir)
  
}

# There are lots of vitamin entries
categories <- unique(tree_info$nodeNames)
#6142 unique entries with depth=2
#21401 unique entries with depth=3

vitamin_list <- categories[grep("[v,V]itamin",categories)]
vitamin_info <- tree_info[grep("[v,V]itamin",tree_info$nodeNames),]

unique(vitamin_list)
length(unique(vitamin_list))
# length=25 with depth=2
# length=104 with depth=3
hormone_list <- categories[grep("[h,H]ormone",categories)]
hormone_list
unique(hormone_list)
length(unique(hormone_list))
# length=34 with depth=2
# length=131 with depth=3

# a less useful example
hydrocarbon_list <- categories[grep("[h,H]ydrocarbon",categories)]
hydrocarbon_list
unique(hydrocarbon_list)
length(unique(hydrocarbon_list))
# length=17 with depth=2
# length=35 with depth=3

# blood - difficult ... mix of drugs and matrices
blood_list <- categories[grep("[b,B]lood",categories)]
blood_list
unique(blood_list)
length(unique(blood_list))
# length=16 with depth=2
# length=59 with depth=3

# try multi-grep?
drug_patterns <- c("[D,d]rug","[P,p]harmaceutical","*[M,m]edication","WHO ATC",
                   "WHO Anatomical")
# drug_list <- categories[grep(drug_patterns,categories)]
# i <- 3
for (i in 1:length(drug_patterns)) {
  temp_list <- categories[grep(drug_patterns[i],categories)]
  if (i == 1) {
    drug_list <- temp_list
  } else if (!is.na(temp_list[1])) {
    drug_list <- c(temp_list,drug_list)
  }
}
unique(drug_list)
length(unique(drug_list))
# length=128 for depth=2
# length=233 for depth=3

agro_patterns <- c("[A,a]grochem","[P,p]esticide","SWISSPEST","[P,p]est",
                   "[I,i]nsecticide","[F,f]ungicide")
# the fancy pesticide regex doesn't work, should catch SWISSPEST but doesn't
for (i in 1:length(agro_patterns)) {
  temp_list <- categories[grep(agro_patterns[i],categories)]
  if (i == 1) {
    agro_list <- temp_list
  } else if (!is.na(temp_list[1])) {
    agro_list <- c(temp_list,agro_list)
  }
}
length(agro_list)
agro_list

unique(agro_list)
length(unique(agro_list))
# length=29 with depth=2
# length=96 with depth=3

#### try to extract data via cache key with hid and hnid ####

#https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?format=json&hid=101&hnid=4041191&cache_uid_type=cid

#https://pubchem.ncbi.nlm.nih.gov/list_gateway/list_gateway.cgi?format=json&action=cache_get&cache_key=UhX0mAgqbZZavG-l7d0mtIuf2v99qaTN3ui_gcX5rYDF4JE

# try to run these over vitamin_info

i <- 1
vitamin_info$nodeCIDs <- ""
#for (i in 1:3) {
for (i in 1:length(vitamin_info$hid)) {
  hid <- vitamin_info$hid[i]
  hnid <- vitamin_info$Node[i]
  node_hnid <- vitamin_info$nodeHNID[i]
  cache_key <- getPcHidCacheID(hid,node_hnid)
  if (!is.na(cache_key)) {
    row_cids <- getPcListCache(cache_key)
    vitamin_info$nodeCIDs[i] <- paste(row_cids,collapse="|")
  } else {
    row_cids <- NULL
  }
  
  if (i == 1) {
    cids <- row_cids
  } else if (length(row_cids)>0) {
    cids <- c(row_cids,cids)
  }
}
length(cids)
unique_cids <- unique(cids)
length(unique_cids)

write.csv(vitamin_info,"vitamin_info.csv",row.names = F)
write.table(sort(unique_cids),"vitamin_unique_CIDs.txt",row.names = F,col.names=F)
# Evan would like two col file: keyword cid and then merge many keyword files together
write.table(cbind("vitamin",sort(unique_cids)),"vitamin_unique_CIDs_keyword.txt",
            row.names = F,col.names=F,quote = F)

# now functionalize this
kw_info_filename <- paste0(kw_info_dir,"Agrochemical_keyword_summary.csv")
getKWInfoCIDs(kw_info_filename)
kw_info_dir <- "C:/DATA/PubChem/keywords/kw_info/"
kw_info_filenames <- list.files(kw_info_dir, full.names = T)
for (i in 1:length(kw_info_filenames)) {
  getKWInfoCIDs(kw_info_filenames[i])
}
kw_cid_dir <- "C:/DATA/PubChem/keywords/kw_cid_files/"
kw_cid_filenames <- list.files(kw_cid_dir,full.names = T)
i <- 2
for (i in 1:length(kw_cid_filenames)){
  kw_cid_info <- read.table(kw_cid_filenames[i],sep=" ")
  if (i == 1) {
    all_kw_cid_info <- kw_cid_info
  } else if (!is.null(kw_cid_info)) {
    all_kw_cid_info <- merge(all_kw_cid_info,kw_cid_info,all=T)
  }
}
all_kw_cid_filename <- paste0(kw_dir,"all_keyword_cids.txt")
write.table(all_kw_cid_info,all_kw_cid_filename,
            row.names = F,col.names=F,quote = F)

kw_cid_dir <- "C:/DATA/PubChem/keywords/kw_cid_files/"
kw_dir <- "C:/DATA/PubChem/keywords/"
createKwCIDfile(kw_cid_dir,kw_dir,"testing_kw_filegen.txt")

# This takes a folder of all the keyword CID files from getKWInfoCIDs
# and merges contents into one big file just for Evan .. 
createKwCIDfile <- function(kw_cid_dir,kw_dir,all_kw_cid_filename="") {
  kw_cid_filenames <- list.files(kw_cid_dir,full.names = T)

  for (i in 1:length(kw_cid_filenames)){
    kw_cid_info <- read.table(kw_cid_filenames[i],sep=" ")
    if (i == 1) {
      all_kw_cid_info <- kw_cid_info
    } else if (!is.null(kw_cid_info)) {
      all_kw_cid_info <- merge(all_kw_cid_info,kw_cid_info,all=T)
    }
  }
  if (nchar(all_kw_cid_filename)<3) {
    all_kw_cid_filename <- paste0(kw_dir,"all_keyword_cids.txt")
  }
  
  write.table(all_kw_cid_info,all_kw_cid_filename,
              row.names = F,col.names=F,quote = F)
  return(all_kw_cid_filename)
}

# input: kw_info_list
# output: kw cid file
# if addCIDs=TRUE this overwrites the source file with the same info plus CIDs
# NOTE this function runs quite long as it runs many queries
# add a print to give updates at n=10?
getKWInfoCIDs <- function(kw_info_filename,addCIDs=FALSE) {
  kw <- strsplit(basename(kw_info_filename),"_")[[1]][1]
  kw_info <- read.csv(kw_info_filename,stringsAsFactors = F)
  kw_cid_dir <- paste0(getwd(),"/kw_cid_files/")
  
  if (!dir.exists(kw_cid_dir)) {
    dir.create(kw_cid_dir)
  }
  
  kw_info$nodeCIDs <- ""
  for (i in 1:length(kw_info$hid)) {
    hid <- kw_info$hid[i]
    hnid <- kw_info$Node[i]
    node_hnid <- kw_info$nodeHNID[i]
    cache_key <- getPcHidCacheID(hid,node_hnid)
    if (!is.na(cache_key)) {
      row_cids <- getPcListCache(cache_key)
      kw_info$nodeCIDs[i] <- paste(row_cids,collapse="|")
    } else {
      row_cids <- NULL
    }
    
    if (i == 1) {
      cids <- row_cids
    } else if (length(row_cids)>0) {
      cids <- c(row_cids,cids)
    }
  }
  #length(cids)
  unique_cids <- unique(cids)
  #length(unique_cids)
  
  # we can add an option to add CIDs into the kw_info_files
  if (addCIDs) {
    write.csv(kw_info,kw_info_filename,row.names = F)
  }
  
  #write.table(sort(unique_cids),"vitamin_unique_CIDs.txt",row.names = F,col.names=F)
  # Evan would like two col file: keyword cid and then merge many keyword files together
  kw_cid_filename <- paste0(kw_cid_dir,kw,"_uniqueCIDs_keyword.txt")
  # write.table(cbind(kw,sort(unique_cids)),"vitamin_unique_CIDs_keyword.txt",
  #             row.names = F,col.names=F,quote = F)
  write.table(cbind(kw,sort(unique_cids)),kw_cid_filename,
              row.names = F,col.names=F,quote = F)
  return(kw_cid_filename)
}


## This function returns a cache ID to get a CID list
getPcListCache <- function(cache_key) {
  baseURL <- "https://pubchem.ncbi.nlm.nih.gov/list_gateway/list_gateway.cgi?"
  #  if (!is.na(depth)) {
  url <- paste0(baseURL, "format=json","&action=cache_get","&cache_key=", cache_key)
  #  } else {
  #    url <- paste0(baseURL, "format=json&hid=", query)
  #  }
  
  errorvar <- 0
  currEnvir <- environment()
  
  tryCatch(
    {#		data <- getURL(URLencode(url),timeout=8),
      res <- GET(URLencode(url))
      data <- httr::content(res, type="text", encoding="UTF-8")
    },
    error=function(e){
      currEnvir$errorvar <- 1
    })
  
  if(errorvar){
    return(NA)
  }
  
  # Otherwise proceed
  r <- fromJSON(data)
  
  if(!is.null(r$Fault))
    return(NA)
  
  # all this information comes with the call
  id_type <- r$ID_Container$identifier_type
  number_ids <- r$ID_Container$number_of_ids
  # but only return this for now
  ids <- r$ID_Container$ids$uint32
  return(ids)
  
}




hid_url <- "https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?format=json&hid=101&hnid=4041191"

hid <- "101"
hnid <- "4041191"
cache_uid <- "cid"
getPcHidCacheID(101,4041191)

getPcListCache(getPcHidCacheID(101,4041191))
getPcListCache(getPcHidCacheID(4,2291199))

## This function returns a cache ID to get a CID list
getPcHidCacheID <- function(hid,hnid,cache_uid="cid") {
  baseURL <- "https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?"
#  if (!is.na(depth)) {
    url <- paste0(baseURL, "format=json","&hid=",hid,"&hnid=", hnid,
                  "&cache_uid_type=",cache_uid)
#  } else {
#    url <- paste0(baseURL, "format=json&hid=", query)
#  }
  
  errorvar <- 0
  currEnvir <- environment()
  
  tryCatch(
    {#		data <- getURL(URLencode(url),timeout=8),
      res <- GET(URLencode(url))
      data <- httr::content(res, type="text", encoding="UTF-8")
    },
    error=function(e){
      currEnvir$errorvar <- 1
    })
  
  if(errorvar){
    return(NA)
  }
  
  exception_check <- grep("Exception during execution: ",data)

  if (length(exception_check)>0 && exception_check==1) {
    warn_string <- paste0("No data file for HID ",hid," and HNID ",hnid)
    warning(warn_string)
    return(NA)
  }
  
  # Otherwise proceed
  r <- fromJSON(data)
  
  if(!is.null(r$Fault))
    return(NA)
  
  cache_key <- r$Hierarchies$CacheKey
  return(cache_key)
  
}


##### Functions and Testing ######
# Functions now ported to hid_tree_functions.R

getPcHidTree("101")
getPcHidTree("72")
getPcHidTree(72,depth=3)
getPcHidTree(3)
getPcHidTree(14,depth = 3)
getPcHidTree(101,depth=3)

# problem with first KEGG tree...
query <- 3 
query <- 72

## this function returns a CSV file containing information about the contents
## of one entire tree, where query = hid number
## depth=2 can be used to specify how deep; this is definitely needed
## for large trees like hid=1 and 2. 
getPcHidTree <- function(query,depth=2)
{
  baseURL <- "https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?"
  if (!is.na(depth)) {
    url <- paste0(baseURL, "format=json","&depth=",depth,"&hid=", query)
  } else {
    url <- paste0(baseURL, "format=json&hid=", query)
  }
  
  errorvar <- 0
  currEnvir <- environment()
  
  tryCatch(
    {#		data <- getURL(URLencode(url),timeout=8),
      res <- GET(URLencode(url))
      data <- httr::content(res, type="text", encoding="UTF-8")
    },
    error=function(e){
      currEnvir$errorvar <- 1
    })
  
  if(errorvar){
    return(NA)
  }
  
  # get the data into useful format
  r <- fromJSON(data)
  
  if(!is.null(r$Fault))
    return(NA)
  
  dat <- r$Hierarchies$Hierarchy[[1]]
  hid <- dat$HID
  source_name <- dat$SourceName
  sourceID <- dat$SourceID
  hnid <- dat$Information$HNID
  # to extract as numbers (but this won't help as they are listed as node_XXX)
  # childIDs <- as.numeric(sub("node_","",dat$Information$ChildID))
  childIDs <- dat$Information$ChildID
  # this is assuming CIDs are always first and SIDs second
  #otherwise this would have to test the "Type"
  n_cids <- dat$Information$Counts[[1]]$Count
  if (length(dat$Information$Counts)>1) {
    # then maybe we have SIDs too
    n_sids <- dat$Information$Counts[[2]]$Count
  } else {
    n_sids <- 0
  }
  
  
  n_nodes <- length(dat$Node)
  
  #nodeNames <- which(unlist(lapply(dat$Node, function(i) !is.null(i$Information$Name))))
  nodeNames <- unlist(lapply(dat$Node, function(i) i$Information$Name))
  # for KEGG we seem to have Description not Names
  nodeDesc <- unlist(lapply(dat$Node, function(i) i$Information$Description))
  nodeHNID <- unlist(lapply(dat$Node, function(i) i$Information$HNID))
  # now test if nodeNames is NULL, if so, replace with Desc
  if (is.null(nodeNames) && !is.null(nodeDesc)) {
    nodeNames <- nodeDesc
  } else if (is.null(nodeNames) && is.null(nodeDesc)) {
    warn_msg <- paste0("Neither Name nor Description available for Nodes for ",
                       "hid ",query)
    warning(warn_msg)
    nodeNames <- vector(mode="character",length=length(nodeHNID))
  }
  # not all entries have CIDs
  i_node_nCIDs <- which(unlist(lapply(dat$Node, function(i) !is.null(i$Information$Counts[[1]]$Count))))
  node_nCIDs <- unlist(lapply(dat$Node, function(i) i$Information$Counts[[1]]$Count))
  
  nodeIDs <- unlist(lapply(dat$Node, function(i) i$NodeID))
  parentIDs <- unlist(lapply(dat$Node, function(i) i$ParentID))
  #sapply(NodeText, function(x)dat$Node[[x]]$Information$Name)
  #dat$Node[[]]
  
#  hid_info <- cbind(nodeNames,nodeHNID,node_nCIDs,nodeIDs,parentIDs)
  hid_info <- as.data.frame(cbind(nodeNames,nodeHNID,nodeIDs,parentIDs))
  # now add in global data
  hid_info$hid <- hid
  hid_info$SourceName <- source_name
  hid_info$SourceID <- sourceID
  hid_info$HNID <- hnid
  # now add nCIDs
  hid_info$node_nCIDs <- 0
  
  for (i in 1:length(i_node_nCIDs)) {
    index <- i_node_nCIDs[i]
    hid_info$node_nCIDs[index] <- node_nCIDs[i]
  }
  
  #do some reordering
  hid_info <- hid_info[,c(5:8,1:4,9)]

  if (!is.na(depth)) {
    export_name <- paste0("classification_tree_hid",query,"_depth",depth,"_export.csv")
  } else {
    export_name <- paste0("classification_tree_hid",query,"_export.csv")
  }
  #export_name <- paste0("classification_tree_hid",query,"_export.csv")
  write.csv(hid_info,export_name,row.names = F)
  
  if(is.null(hid_info)){
    return(NA)
  } else{
    return(export_name)
  }
  
}

# # Legacy testing now commented out (slow entries resolved)
# # getting the tree summary bit by bit to test which ones are slow
# # (seems to be only 1 & 2 ... now done manually ... )
# getPcHidTreeList(100,102)
# getPcHidTreeList(1,5)
# # really slow for hid=1,2
# #getPcHidTreeList(80,100)
# getPcHidTreeList(90,92)
# getPcHidTreeList(90,105)
# getPcHidTreeList(80,89)
# getPcHidTreeList(60,79)
# getPcHidTreeList(30,59)
# getPcHidTreeList(10,29)
# getPcHidTreeList(3,9)
# getPcHidTreeList(1,9) # testing manual entries for 1 & 2

getPcHidTreeList() #gets 1 to 103

## This function returns a csv file (name returned) containing
## basic information about all classification trees in the specified range
## The csv output contains this basic info, NA is the hid does not exist
## depth is needed especially for large trees (hid=1, 2)
getPcHidTreeList <- function(hid_start=1,hid_stop=103,depth=2) {
  # this function checks the URLs and returns a file with listing
  # NOTE: HIDs 1 and 2 are too slow and are done manually
  # HID1 takes ages ... HID 2 returns bad gateway(?!)
  # NB this is fixed with the "depth=2" parameter!
  queries <- hid_start:hid_stop
  # set up vectors
  #n_hids <- (hid_stop-hid_start)+1
  n_hids <- length(queries)
  hids <- vector("numeric",length=n_hids)
  source_names <- vector("character",length=n_hids)
  sourceIDs <- vector("character",length=n_hids)
  tree_names <- vector("character",length=n_hids)
  hnids <- vector("numeric",length=n_hids)
  hasCIDs <- vector("logical",length=n_hids)
  
  # run loop
  for (i in 1:n_hids) {
    query <- queries[i]
    tree_info <- getPcHidTreeInfo(query,depth=depth)
    
    # if (query==1) {
    #   tree_info <- list()
    #   tree_info[['HID']] <- 1
    #   tree_info[['SourceName']] <- "MeSH"
    #   tree_info[['SourceID']] <- "DescTree"
    #   tree_info[['TreeName']] <- "MeSH Tree"
    #   tree_info[['HNID']] <- 1
    #   tree_info[['hasCIDs']] <- TRUE
    #   
    # } else if (query==2) {
    #   tree_info <- list()
    #   tree_info[['HID']] <- 2
    #   tree_info[['SourceName']] <- "ChEBI"
    #   tree_info[['SourceID']] <- NA
    #   tree_info[['TreeName']] <- NA
    #   tree_info[['HNID']] <- NA
    #   tree_info[['hasCIDs']] <- TRUE
    #   
    # } else {
    #   tree_info <- getPcHidTreeInfo(query,depth=depth)
    # }
    
    if (!is.na(tree_info[[1]])) {
      hids[i] <- tree_info$HID
      source_names[i] <- tree_info$SourceName
      sourceIDs[i] <- tree_info$SourceID
      tree_names[i] <- tree_info$TreeName
      hnids[i] <- tree_info$HNID
      if(length(tree_info$hasCIDs)>0) {
        hasCIDs[i] <- tree_info$hasCIDs
      } else {
        hasCIDs[i] <- FALSE
      }
      
    } else {
      hids[i] <- query
      source_names[i] <- NA
      sourceIDs[i] <- NA
      tree_names[i] <- NA
      hnids[i] <- NA
      hasCIDs[i] <- NA
    }
  }
  # now merge together
  tree_summary <- cbind(hids,source_names,sourceIDs,tree_names,hnids,hasCIDs)
  colnames(tree_summary) <- c("HID","SourceName","SourceID","TreeName",
                              "HNID","hasCIDs")
  hid_file_name <- paste0("HID_Tree_Summary_",hid_start,"_to_",hid_stop,".csv")
  write.csv(tree_summary,hid_file_name,row.names = F)
  return(hid_file_name)
  
}

getPcHidTreeInfo(101)
getPcHidTreeInfo(72)
getPcHidTreeInfo(300)
# without depth, this is really, really slow for hid=1, 2 
getPcHidTreeInfo(1)

tree_info <- getPcHidTreeInfo(101)

## This function returns basic information about a single classification 
## tree, returning the output as a list of this basic info
## Feeds into getPcHidTreeList
## depth is needed especially for large trees (hid=1, 2)
# tree_info[['HID']] <- hid
# tree_info[['SourceName']] <- source_name
# tree_info[['SourceID']] <- sourceID
# tree_info[['TreeName']] <- tree_name
# tree_info[['HNID']] <- hnid
# tree_info[['hasCIDs']] <- hasCIDs
getPcHidTreeInfo <- function(query, depth=2)
{
  baseURL <- "https://pubchem.ncbi.nlm.nih.gov/classification/cgi/classifications.fcgi?"
  if (!is.na(depth)) {
    url <- paste0(baseURL, "format=json","&depth=",depth,"&hid=", query)
  } else {
    url <- paste0(baseURL, "format=json&hid=", query)
  }
  
  errorvar <- 0
  currEnvir <- environment()
  
  tryCatch(
    {#		data <- getURL(URLencode(url),timeout=8),
      res <- GET(URLencode(url))
      data <- httr::content(res, type="text", encoding="UTF-8")
    },
    error=function(e){
      currEnvir$errorvar <- 1
    })
  
  if(errorvar){
    return(NA)
  }
  
  exception_check <- grep("Exception during execution: NCBI",data)
  
  if (length(exception_check)>0 && exception_check==1) {
    warn_string <- paste0("No data file for HID ",query)
    warning(warn_string)
    return(NA)
  }
  
  # Otherwise proceed
  r <- fromJSON(data)
  
  if(!is.null(r$Fault))
    return(NA)
  
  dat <- r$Hierarchies$Hierarchy[[1]]
  hid <- dat$HID
  if (!(as.character(hid)==as.character(query))){
    warn_string <- paste0("Query hid ",query," and retrieved hid ",hid," don't match")
    warning(warn_string)
  }
  source_name <- dat$SourceName
  sourceID <- dat$SourceID
  tree_name <- dat$Information$Name
  hnid <- dat$Information$HNID
  hasCIDs <- which(dat$Information$Counts[[1]]$Type == "CID")>0
  
  tree_info <- list()
  tree_info[['HID']] <- hid
  tree_info[['SourceName']] <- source_name
  tree_info[['SourceID']] <- sourceID
  tree_info[['TreeName']] <- tree_name
  tree_info[['HNID']] <- hnid
  tree_info[['hasCIDs']] <- hasCIDs
  
  return(tree_info)
  
}


